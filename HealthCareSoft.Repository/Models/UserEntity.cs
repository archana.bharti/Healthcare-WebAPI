﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Repository.Models
{
   public class UserEntity
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddelName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }       
        public string Password { get; set; }      
        public string Gender { get; set; }
        public string ProfilePicture { get; set; }
        public bool IsActive { get; set; }
        public int RoleId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long Otp { get; set; }
        public Nullable<int> UserPin { get; set; }
        public Nullable<long> BloodGroupId { get; set; }
        public Nullable<long> SpecializationId { get; set; }
    }
}
