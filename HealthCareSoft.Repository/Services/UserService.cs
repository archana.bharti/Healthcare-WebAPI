﻿using AutoMapper;
using HealthCareSoft.Entity;
using HealthCareSoft.Entity.UnitOfWork;
using HealthCareSoft.Repository.Interfaces;
using HealthCareSoft.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;


namespace HealthCareSoft.Repository.Services
{

    public class UserService : IUserServices
    {
        private readonly UnitOfWork _unitOfWork;
        public UserService()
        {
            _unitOfWork = new UnitOfWork();
        }
        public UserEntity GetUserById(int userId)
        {
            var user = _unitOfWork.UserRepository.GetByID(userId);
            if (user != null)
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                var productModel = Mapper.Map<UserProfile, UserEntity>(user);
                return productModel;
            }
            return null;
        }
        public UserEntity GetUserByName(string userId,string password)
        {

            var user = _unitOfWork.UserRepository.Get(u => u.Email == userId && u.Password == password);
            if (user != null)
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                var productModel = Mapper.Map<UserProfile, UserEntity>(user);
                return productModel;
            }
            return null;
        }
        public bool GetUserByUserName(string uname)
        {
            bool result = true;
            var user = _unitOfWork.UserRepository.Get(u => u.FirstName == uname);
            if (user != null)
            result = false;
           return result;
        }
        public bool GetUserByEmail(string email)
        {
            bool result = true;
            var user = _unitOfWork.UserRepository.Get(u => u.Email == email);
            if (user != null)
                result = false;
            return result;
        }
        public bool DeleteUser(int userId)
        {
            var success = false;
            if (userId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.UserRepository.GetByID(userId);
                    if (user != null)
                    {

                        _unitOfWork.UserRepository.Delete(user);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool UserStatus(int userId)
        {
            var success = false;
            if (userId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.UserRepository.GetByID(userId);
                    if (user != null)
                    {
                        user.Id = userId;
                        if (user.IsActive == false)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            user.IsActive = false;
                        }
                        try
                        {
                            _unitOfWork.UserRepository.Update(user);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }
            return success;
        }
        public IEnumerable<UserEntity> GetAllUsers()
        {
            var users = _unitOfWork.UserRepository.GetAll().ToList();
            if (users.Any())
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                //Mapper.CreateMap<User, UserEntity>().ForMember(user => user.UserStatus,
                //            map => map.MapFrom(entity => entity.Status));
                var usersModel = Mapper.Map<List<UserProfile>, List<UserEntity>>(users);
                return usersModel;
            }
            return null;
        }
        public IEnumerable<UserEntity> GetAllSubAdmin()
        {
            var users = _unitOfWork.UserRepository.GetManyQueryable(m => m.RoleId == 4).ToList();
            if (users.Any())
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                //Mapper.CreateMap<User, UserEntity>().ForMember(user => user.UserStatus,
                //            map => map.MapFrom(entity => entity.Status));
                var usersModel = Mapper.Map<List<UserProfile>, List<UserEntity>>(users);
                return usersModel;
            }
            return null;
        }
        public IEnumerable<UserEntity> GetAllDoctors()
        {
            var users = _unitOfWork.UserRepository.GetManyQueryable(m => m.RoleId ==1).ToList();
            if (users.Any())
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                //Mapper.CreateMap<User, UserEntity>().ForMember(user => user.UserStatus,
                //            map => map.MapFrom(entity => entity.Status));
                var usersModel = Mapper.Map<List<UserProfile>, List<UserEntity>>(users);
                return usersModel;
            }
            return null;
        }
        public IEnumerable<UserEntity> GetAllPatients()
        {
            var users = _unitOfWork.UserRepository.GetManyQueryable(m => m.RoleId == 3).ToList();
            if (users.Any())
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                //Mapper.CreateMap<User, UserEntity>().ForMember(user => user.UserStatus,
                //            map => map.MapFrom(entity => entity.Status));
                var usersModel = Mapper.Map<List<UserProfile>, List<UserEntity>>(users);
                return usersModel;
            }
            return null;
        }
        public long CreateUser(UserEntity userEntity)
        {
            using (var scope = new TransactionScope())
            {
                var user = new UserProfile
                {
                    FirstName = userEntity.FirstName,
                    Email= userEntity.Email,
                    Password= userEntity.Password,
                    MiddelName = userEntity.MiddelName,
                    LastName = userEntity.LastName,
                    Address = userEntity.Address,
                    PostalCode = userEntity.PostalCode,
                    PhoneNo = userEntity.PhoneNo,
                    Gender = userEntity.Gender,
                    ProfilePicture = userEntity.ProfilePicture,
                    IsActive = userEntity.IsActive,
                    RoleId = userEntity.RoleId,
                    CreatedBy=userEntity.CreatedBy
                                   

                };
                try
                {
                    _unitOfWork.UserRepository.Insert(user);
                    _unitOfWork.Save();
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return -1;
                }
                return user.Id;
            }
        }
        public bool UpdateUser(long userId, UserEntity userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.UserRepository.GetByID(userId);
                    if (user != null)
                    {
                       // user.UserId = userEntity.UserId;
                        user.Email = userEntity.Email;
                        user.Password = userEntity.Password;
                        user.FirstName = userEntity.FirstName;
                        //user.Email = userEntity.Email,
                        //Password = userEntity.Password,
                        user.MiddelName = userEntity.MiddelName;
                        user.LastName = userEntity.LastName;
                        user.Address = userEntity.Address;
                        user.PostalCode = userEntity.PostalCode;
                        user.PhoneNo = userEntity.PhoneNo;
                        user.Gender = userEntity.Gender;
                        user.ProfilePicture = userEntity.ProfilePicture;
                        user.IsActive = userEntity.IsActive;
                        user.RoleId = userEntity.RoleId;
                        user.CreatedBy = userEntity.CreatedBy;
                        try
                        {
                            _unitOfWork.UserRepository.Update(user);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }

            return success;
        }
        public UserEntity GetUserDetailByEmail(string email)
        {
            var user = _unitOfWork.UserRepository.Get(u => u.Email == email);
            if (user != null)
            {
                Mapper.CreateMap<UserProfile, UserEntity>();
                var userModel = Mapper.Map<UserProfile, UserEntity>(user);
                return userModel;
            }
            return null;
        }

        public bool ResetPassword(long userId,string email,string password)
        {
            var success = false;
            if(email!=null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.UserRepository.GetByID(userId);
                    if (user != null)
                    {                        
                        user.Password = password;
                        try
                        {
                            _unitOfWork.UserRepository.Update(user);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                    }
                }
            return success;
        }
    }

//------------------ Manage Hospital---------------->
    public class HospitalService : IHospitalServices
    {
        private readonly UnitOfWork _unitOfWork;
        public HospitalService()
        {
            _unitOfWork = new UnitOfWork();
        }

        public HospitalEntity GetHospitalById(long HospitalId)
        {
            var Hospital = _unitOfWork.HospitalRepository.GetByID(HospitalId);
            if (Hospital != null)
            {
                Mapper.CreateMap<Hospital, HospitalEntity>();
                var productModel = Mapper.Map<Hospital, HospitalEntity>(Hospital);
                return productModel;
            }
            return null;
        }

        public bool HospitalStatus(int HospitalId)
        {
            var success = false;
            if (HospitalId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var Hospital = _unitOfWork.HospitalRepository.GetByID(HospitalId);
                    if (Hospital != null)
                    {
                        Hospital.Id = HospitalId;
                        if (Hospital.IsAcctive == false)
                        {
                            Hospital.IsAcctive = true;
                        }
                        else
                        {
                            Hospital.IsAcctive = false;
                        }
                        try
                        {
                            _unitOfWork.HospitalRepository.Update(Hospital);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool DeleteHospital(int HospitalId)
        {
            var success = false;
            if (HospitalId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var Hospital = _unitOfWork.HospitalRepository.GetByID(HospitalId);
                    if (Hospital != null)
                    {

                        _unitOfWork.HospitalRepository.Delete(Hospital);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public IEnumerable<HospitalEntity> GetAllHospital()
        {
            var Hospitals = _unitOfWork.HospitalRepository.GetAll().ToList();
            if (Hospitals.Any())
            {
                Mapper.CreateMap<Hospital, HospitalEntity>();
                var CountrysModel = Mapper.Map<List<Hospital>, List<HospitalEntity>>(Hospitals);
                return CountrysModel;
            }
            return null;
        }
        public long CreateHospital(HospitalEntity ObjEntity)
        {
            using (var scope = new TransactionScope())
            {
                var Hospital = new Hospital
                {
                    HospitalName = ObjEntity.HospitalName,
                    Address=ObjEntity.Address,
                    Latitude=ObjEntity.Latitude,
                    Longitude=ObjEntity.Longitude,
                     IsAcctive=ObjEntity.IsAcctive,
                     CreatedBy=ObjEntity.CreatedBy


                };
                try
                {
                    _unitOfWork.HospitalRepository.Insert(Hospital);
                    _unitOfWork.Save();
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return -1;
                }
                return Hospital.Id;
            }
        }
        public bool UpdateHospital(long HospitalId, HospitalEntity ObjEntity)
        {
            //var obj= _unitOfWork.CountryRepository.SQLQuery<CountryEntity>("EXEC mySP @p1", new SqlParameter("@p1", "value"));
            var success = false;
            if (ObjEntity != null)
            {
                using (var scope = new TransactionScope())
                {                   
                    var Hospital = _unitOfWork.HospitalRepository.GetByID(HospitalId);
                    if (Hospital != null)
                    {
                        //.CountryId = CountryEntity.CountryId;
                        Hospital.HospitalName = ObjEntity.HospitalName;
                        // HospitalName = ObjEntity.HospitalName,
                        Hospital.Address = ObjEntity.Address;
                    Hospital.Latitude = ObjEntity.Latitude;
                    Hospital.Longitude = ObjEntity.Longitude;
                     Hospital.IsAcctive = ObjEntity.IsAcctive;
                        Hospital.CreatedBy = ObjEntity.CreatedBy;
                        try
                        {
                            _unitOfWork.HospitalRepository.Update(Hospital);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }

            return success;
        }
       public bool GetHospitalByLocation(decimal latitude, decimal longitude)
        {

            bool result = true;
            var user = _unitOfWork.HospitalRepository.Get(u =>u.Latitude==latitude && u.Longitude==longitude);
            if (user != null)
            {
                result = false;
            }               
            return result;
        }
    }
    //---------------------Manage Department------------------>
    public class DepartmentService : IDepartmentServices
    {
        private readonly UnitOfWork _unitOfWork;

        public DepartmentService()
        {
            _unitOfWork = new UnitOfWork();
        }


        public DepartmentEntity GetDepartmentById(long DepartmentId)
        {
            var Dept = _unitOfWork.DepartmentRepository.GetByID(DepartmentId);
            if (Dept != null)
            {
                Mapper.CreateMap<Department, DepartmentEntity>();
                var productModel = Mapper.Map<Department, DepartmentEntity>(Dept);
                return productModel;
            }
            return null;
        }
        public bool DeleteDepartment(long DepartmentId)
        {
            var success = false;
            if (DepartmentId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var Dept = _unitOfWork.DepartmentRepository.GetByID(DepartmentId);
                    if (Dept != null)
                    {

                        _unitOfWork.DepartmentRepository.Delete(Dept);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public IEnumerable<DepartmentEntity> GetAllDepartment()
        {
            var Depts = _unitOfWork.DepartmentRepository.GetAll().ToList();
            if (Depts.Any())
            {
                Mapper.CreateMap<Department, DepartmentEntity>();
                var DeptsModel = Mapper.Map<List<Department>, List<DepartmentEntity>>(Depts);
                return DeptsModel;
            }
            return null;
        }
        public long CreateDepartment(DepartmentEntity ObjEntity)
        {
            using (var scope = new TransactionScope())
            {
                var Dept = new Department
                {
                    Name = ObjEntity.Name,
                    IsActive = ObjEntity.IsActive,
                    CreatedBy = ObjEntity.CreatedBy
                };
                try
                {
                    _unitOfWork.DepartmentRepository.Insert(Dept);
                    _unitOfWork.Save();
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return -1;
                }
                return Dept.Id;
            }
        }
        public bool UpdateDepartment(long DepartmentId, DepartmentEntity ObjEntity)
        {
            var success = false;
            if (ObjEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var Dept = _unitOfWork.DepartmentRepository.GetByID(DepartmentId);
                    if (Dept != null)
                    {
                        //.CityId = CityEntity.CityId;
                        Dept.Name = ObjEntity.Name;
                        Dept.IsActive = ObjEntity.IsActive;
                        Dept.CreatedBy = ObjEntity.CreatedBy;
                        try
                        {
                            _unitOfWork.DepartmentRepository.Update(Dept);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }

            return success;
        }
        public bool DepartmentStatus(long DepartmentId)
        {
            var success = false;
            if (DepartmentId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var Dept = _unitOfWork.DepartmentRepository.GetByID(DepartmentId);
                    if (Dept != null)
                    {
                        Dept.Id = DepartmentId;
                        if (Dept.IsActive == false)
                        {
                            Dept.IsActive = true;
                        }
                        else
                        {
                            Dept.IsActive = false;
                        }
                        try
                        {
                            _unitOfWork.DepartmentRepository.Update(Dept);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }
            return success;
        }
    }



    public class EnquiryService : IEnquiryServices
    {
        private readonly UnitOfWork _unitOfWork;
        public EnquiryService()
        {
            _unitOfWork = new UnitOfWork();
        }
        public EnquiryEntity GetEnquiryById(long EnquiryId)
        {
            var Enqiry = _unitOfWork.EnquiryRepository.GetByID(EnquiryId);
            if (Enqiry != null)
            {
                Mapper.CreateMap<Enqury, EnquiryEntity>();
                var productModel = Mapper.Map<Enqury, EnquiryEntity>(Enqiry);
                return productModel;
            }
            return null;
        }

        public IEnumerable<EnquiryEntity> GetAllEnquiry()
        {
            var Enqirys = _unitOfWork.EnquiryRepository.GetAll().ToList();
            if (Enqirys.Any())
            {
                Mapper.CreateMap<Enqury, EnquiryEntity>();
                var CMSPagesModel = Mapper.Map<List<Enqury>, List<EnquiryEntity>>(Enqirys);
                return CMSPagesModel;
            }
            return null;
        }
        public bool UpdateEnquiry(long EnquiryId, EnquiryEntity ObjEntity)
        {
            //using (var scope = new TransactionScope())
            //{
            //    var CMSPage = new Enqury
            //    {
            //        Name = CMSPageEntity.PageName
            //    };
            //    try
            //    {
            //        _unitOfWork.CMSPageRepository.Insert(CMSPage);
            //        _unitOfWork.Save();
            //        scope.Complete();
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //        return -1;
            //    }
            //    return CMSPage.CMSId;
            //}
            return false;
        }

      public  bool EnquiryStatus(int EnquiryId)
        {
            var success = false;
            if (EnquiryId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var Enqry = _unitOfWork.EnquiryRepository.GetByID(EnquiryId);
                    if (Enqry != null)
                    {
                        Enqry.Id = EnquiryId;
                        if (Enqry.IsAnswered == false)
                        {
                            Enqry.IsAnswered = true;
                        }
                        else
                        {
                            Enqry.IsAnswered = false;
                        }
                        try
                        {
                            _unitOfWork.EnquiryRepository.Update(Enqry);
                            _unitOfWork.Save();
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return success;
                        }
                        success = true;
                    }
                }
            }
            return success;
        }
    }


        
//--------User Login API---------//
      
}