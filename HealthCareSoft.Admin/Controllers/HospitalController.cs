﻿using HealthCareSoft.Admin.Models;
using HealthCareSoft.Repository.Interfaces;
using HealthCareSoft.Repository.Models;
using HealthCareSoft.Repository.Services;
using System;
using System.Linq.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthCareSoft.Admin.Controllers
{
   [CustomAuthorize]
    public class HospitalController : Controller
    {
        private readonly IHospitalServices _hospitalservice;
        public HospitalController()
        {
            
            _hospitalservice = new HospitalService();
        }
        // GET: Hospital
        public ActionResult HospitalList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoadHospital()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var search = Request.Form.GetValues("search[value]").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
            var v = (from a in _hospitalservice.GetAllHospital() select a);
            if (!string.IsNullOrEmpty(search))
            {
                v = v.Where(a => a.HospitalName.ToLower().StartsWith(search.ToLower()) || a.HospitalName.ToLower().StartsWith(search.ToLower()));
            }
            // SORT
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                v = v.OrderBy(sortColumn + " " + sortColumnDir);
            }
            recordsTotal = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        //CRUD operations for Users
        public ActionResult HospitalStatus(string id)
        {
            bool result = _hospitalservice.HospitalStatus(Convert.ToInt32(id));
            if (result)
            {
                return RedirectToAction("HospitalList", "Hospital");
            }
            else
            {
                TempData["Delete"] = string.Format("Error Coming While Deleting Data");
                return RedirectToAction("HospitalList", "Hospital");
            }
        }

        [HttpGet]
        public ActionResult EditHospital(string id)
        {
            HospitalEntity data = _hospitalservice.GetHospitalById(Convert.ToInt64(id));
            return PartialView("_UserEdit", data);
        }
        [HttpPost]
        public ActionResult EditHospital(HospitalEntity data)
        {
            data.CreatedBy= ApplicationSession.CurrentUser.Id; 
            try
            {
                if (ModelState.IsValid)
                {
                    bool result = _hospitalservice.UpdateHospital(data.Id, data);
                    if (result)
                    {
                        return Json(new { success = true });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Network Error Try Again ");
                    }
                }
            }
            catch
            {
                ModelState.AddModelError("", "Unable to Save. Try again, and if the problem persists see your system administrator.");// handle for database troble
            }
            return PartialView("_UserEdit", data);
        }

        [HttpGet]
        public ActionResult CreateHospital()
        {
            return PartialView("_UserCreate");
        }
        [HttpPost]
        public ActionResult CreateHospital(HospitalEntity u)
        {
            u.CreatedBy= ApplicationSession.CurrentUser.Id; 
            if (ModelState.IsValid)
            {
                bool uname = _hospitalservice.GetHospitalByLocation(u.Latitude,u.Longitude);
                if (!uname)
                {
                    ModelState.AddModelError("", "User Name Already Exist");
                    return PartialView("_UserCreate", u);
                }
                //bool email = _hospitalservice.GetUserByEmail(u.Email);
                //if (!email)
                //{
                //    ModelState.AddModelError("", "Email Already Exist");
                //    return PartialView("_UserCreate", u);
                //}
                long result = _hospitalservice.CreateHospital(u);
                if (result > 0)
                {
                    return Json(new { success = true });
                }
                else
                {
                    ModelState.AddModelError("", "Network Error Try Again ");
                }
            }
            return PartialView("_UserCreate", u);
        }

        public JsonResult DeleteHospital(string id)
        {
            bool result = _hospitalservice.DeleteHospital(Convert.ToInt32(id));
            if (result)
            {
                return Json("Successfully deleted Record !");
            }
            else
            {
                TempData["Delete"] = string.Format("Error Coming While Deleting Data");
                return Json("Error Coming While Deleting Data");
            }
        }
    }
}