﻿using HealthCareSoft.API.Filter;
using HealthCareSoft.API.Models;
using HealthCareSoft.Entity;
using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using HealthCareSoft.Entity.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HealthCareSoft.API.Controllers
{
    [RoutePrefix("api/Appoinment")]
    public class AppoinmentController : ApiController
    {
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        private HttpResponseMessage _response;
        private IAppoinmentRepository _objIAppoinmentRepository;
        public AppoinmentController()
        {
            _objIAppoinmentRepository = new AppoinmentRepository();
        }

        // GET: api/BookAppoinment

        [HttpPost]
        [SecureResource]
        [Route("BookAppoinment")]
        
        public HttpResponseMessage BookAppoinmemtByPatient(BookAppointmentRequestModel objBookAppointmentRequestModel)
        {
            BookAppointmentResposeModel result = new BookAppointmentResposeModel();
            try
            {
                

                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();
               
                int value = _objIAppoinmentRepository.BookAppoinment(objBookAppointmentRequestModel);

                if (value>0)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Your Booking has been confirmed at the selected time.!";
                    result.Response.Id = UserId;
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";

                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception Ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = Ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }


        [HttpPost]
        [SecureResource]
        [Route("AppoinmentList")]
        public HttpResponseMessage AppoinmemtListOfPatient(AppointmentListRequestModel objAppointmentListRequestModel)
        {
            AppointmentListResponseModel result = new AppointmentListResponseModel();             
            try
            {               
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();              
                
                result.Response.AppointmentList = _objIAppoinmentRepository.GetAppointmentListOfPatient(UserId, objAppointmentListRequestModel.ListType, objAppointmentListRequestModel.Key).ToList();
                
                foreach (var item in result.Response.AppointmentList)
                {
                    item.DoctorProfilePic = CommonUrl.GetUrlAPI + item.DoctorProfilePic;
                    if (item.DoctorProfilePic == null)
                    {
                        item.DoctorProfilePic = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                    }

                }

                if (result!=null)
                {
                    
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";

                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception Ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = Ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }
    }
}
