﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HealthCareSoft.Entity;
using System.IO;
using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using HealthCareSoft.Entity.Repository;
using HealthCareSoft.API.Filter;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Data.SqlClient;
using HealthCareSoft.API.Models;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.Cryptography;

namespace HealthCareSoft.API.Controllers
{
    [RoutePrefix("api/User")]
    public class UserSettingsController : ApiController
    {
        private string _profileImagesPath = WebConfigurationManager.AppSettings["ProfileImages"];
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        private IUserSettings _objIUserSettings;
        private HttpResponseMessage _response;

        public UserSettingsController()
        {
            _objIUserSettings = new UserSettings();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login(LoginRequestModel objLoginRequestModel)
        {
            UserProfileModel objUserProfile = null;
            LoginResponseModel result = new LoginResponseModel();
            try
            {

                objUserProfile = _objIUserSettings.Login(objLoginRequestModel);
                Int64 Id = _objHealthCareEntities.Database.SqlQuery<Int64>("select Id from UserProfile where PhoneNo={0}", objLoginRequestModel.PhoneNo).FirstOrDefault();


                string name = _objHealthCareEntities.Database.SqlQuery<string>("select FirstName from UserProfile where Id={0}", Id).FirstOrDefault();


                if (objUserProfile != null)
                {

                    Random random = new Random();
                    //Int64 otp = 1234;

                    Int64 otp = Convert.ToInt64(random.Next(1000, 9999)); /// to specify range for random number
                    string message = "Dear name, Thank you for registering with Stetho - Patient Connect. Your OTP is otp";
                    message = message.Replace("name", name);
                    message = message.Replace("otp", otp.ToString());

                    string response = sendSMS(message, objLoginRequestModel.PhoneNo);
                    int UpdatedStatus = _objIUserSettings.UpdateOtp(otp, objLoginRequestModel.PhoneNo);

                    if (UpdatedStatus > 0)
                    {
                        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                        result.Response.Message = "Message has been sent to the registered mobile...";
                        result.Response.UserId = Id;
                    }
                    else
                    {
                        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.ExpectationFailed);
                        result.Response.Message = "Message Service is not Responding please try later...";
                    }
                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Mobile number is not registered...";
                }
                _response = Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return _response;
        }

        public string sendSMS(string Message, string Phoneno)
        {

            String result;
            string apiKey = "jI+Qerjdctc-jh43E0wUa3ZiwrfeiuAiNM1piwrjSj";
            string numbers = Phoneno; // in a comma seperated list
            string message = Message;
            string sender = "STETHO";
            //string url = "https://api.textlocal.in/send?apikey=jI+Qerjdctc-jh43E0wUa3ZiwrfeiuAiNM1piwrjSj&sender=STETHO&numbers=917834982290&message=Hello! Welcome to Stetho";
            String url = "https://api.textlocal.in/send?apikey=" + apiKey + "&numbers=" + numbers + "&message=" + message + "&sender=" + sender;
            //refer to parameters to complete correct url string

            StreamWriter myWriter = null;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);

            objRequest.Method = "POST";
            //objRequest.ContentLength = Encoding.UTF8.GetByteCount(url);
            objRequest.ContentType = "application/json";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(url);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                // Close and clean up the StreamReader
                sr.Close();
            }
            return result;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ConfirmOtp")]
        public HttpResponseMessage ConfirmOtp(ConfirmOtpRequestModel objConfirmOtpRequestModel)
        {

            ConfirmOtpResponseModel result = new ConfirmOtpResponseModel();

            try
            {
                result.Response = _objIUserSettings.ConfirmOtp(objConfirmOtpRequestModel);

                if (result.Response != null)
                {
                    string UserToken = _objHealthCareEntities.Database.SqlQuery<string>("select TokenCode from Token where UserId = {0}", objConfirmOtpRequestModel.UserId).FirstOrDefault();

                    if (UserToken == null || UserToken == "0")
                    {
                        Int64 UserTokenId = _objHealthCareEntities.Database.SqlQuery<Int64>("select Id from Token where UserId={0}", objConfirmOtpRequestModel.UserId).FirstOrDefault();
                        if (UserTokenId == 0)
                        {
                            Token objToken = new Token()
                            {
                                UserId = result.Response.Id,
                                RoleId = result.Response.RoleId,
                                CreatedOn = DateTime.Now,
                                IsActive = true,
                                ExpiryDate = DateTime.Now.AddDays(7),
                                TokenCode = Guid.NewGuid().ToString() + result.Response.Id.ToString() + Guid.NewGuid().ToString()
                            };


                            _objHealthCareEntities.Tokens.Add(objToken);
                            _objHealthCareEntities.SaveChanges();
                            result.Response.TokenCode = objToken.TokenCode;
                        }
                        else
                        {
                            Token objToken = new Token()
                            {
                                UserId = result.Response.Id,
                                RoleId = result.Response.RoleId,
                                CreatedOn = DateTime.Now,
                                IsActive = true,
                                ExpiryDate = DateTime.Now.AddDays(7),
                                TokenCode = Guid.NewGuid().ToString() + result.Response.Id.ToString() + Guid.NewGuid().ToString()
                            };


                            _objHealthCareEntities.Tokens.Add(objToken);
                            int rowEffected = _objHealthCareEntities.Database.ExecuteSqlCommand("Update Token set TokenCode=@TokenCode where Id=@Id",
                                                                new SqlParameter("TokenCode", objToken.TokenCode),
                                                                new SqlParameter("Id", UserTokenId));
                            //_objHealthCareEntities.SaveChanges();
                            result.Response.TokenCode = objToken.TokenCode;
                        }

                    }

                    if (result.Response.UserPin == null)
                    {
                        Random random = new Random();
                        int UserPin = Convert.ToInt32(random.Next(1000, 9999)); /// to specify range for random number
                        int rowEffected = _objHealthCareEntities.Database.ExecuteSqlCommand("Update UserProfile set UserPin=@UserPin where Id=@Id",
                            new SqlParameter("UserPin", UserPin),
                            new SqlParameter("Id", result.Response.Id));
                        result.Response.UserPin = UserPin;
                    }
                    if (result.Response.ProfilePicture == null)
                    {
                        result.Response.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";
                    }
                    else
                    {
                        result.Response.ProfilePicture = CommonUrl.GetUrlAPI + result.Response.ProfilePicture;
                    }

                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    _response = Request.CreateResponse(HttpStatusCode.OK, "Otp Mismatch...");
                }
            }
            catch (Exception ex)
            {
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Some error occurred..");
            }
            return _response;
        }


        [AllowAnonymous]
        [SecureResource]
        [HttpPost]
        [Route("UserProfileDetail")]
        public HttpResponseMessage UserProfileDetails(UserProfileDetailsRequestModel objUserProfileDetailsRequestModel)
        {
            UserProfileDetailsResponseModel result = new UserProfileDetailsResponseModel();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response = _objIUserSettings.GetUserDetails(UserId);

                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception Ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = Ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return _response;
        }



        [SecureResource]
        [HttpPost]
        [Route("EditProfile")]
        public HttpResponseMessage EditprofileDetail(UserEditProfileRequestModel objUserEditProfileRequestModel)
        {
            UserEditProfileResponseModel result = new UserEditProfileResponseModel();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                //result.Response = _objIUserSettings.EditProfileDetail(objUserEditProfileRequestModel);
                int value = _objIUserSettings.EditProfileDetail(objUserEditProfileRequestModel);

                if (value > 0)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "User Profile Updated successfully....";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }

            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return _response;
        }

        [SecureResource]
        [HttpPost]
        [Route("Logout")]
        public HttpResponseMessage Logout()
        {
            UserSettingLogoutResponseModel result = new UserSettingLogoutResponseModel();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                //result.Response = _objIUserSettings.LogoutUser();
                int LogoutUser = _objIUserSettings.LogoutUser(UserId);

                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Logout successfully....";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return _response;
        }


        [SecureResource]
        [HttpPost]
        [Route("GetDoctorDetailById")]
        public HttpResponseMessage GetDoctorDetailById(DoctorDetailsByIdRequestModel objDoctorDetailsByIdRequestModel)
        {
            DoctorDetailsByIdResponseModel result = new DoctorDetailsByIdResponseModel();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                //.Response = _objIUserSettings.GetDoctorDetailById();

                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Logout successfully....";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return _response;
        }



        [HttpPost]
        [Route("SignUp")]
        public HttpResponseMessage UserSignUp(SignUpRequestModel objSignUpRequestModel)
        {
            SignUpResponseModel result = new SignUpResponseModel();
            VMResponse res = new VMResponse();
            try
            {
                string MobileNo = _objHealthCareEntities.Database.SqlQuery<string>("select PhoneNo from UserProfile where PhoneNo={0}", objSignUpRequestModel.PhoneNo).FirstOrDefault();
                string mailId = _objHealthCareEntities.Database.SqlQuery<string>("select Email from UserProfile where Email={0}", objSignUpRequestModel.Email).FirstOrDefault();
                if (MobileNo != null && mailId == null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.Ambiguous);
                    result.Response.Message = "This Phone Number already Register..";
                    _response = Request.CreateResponse(HttpStatusCode.Ambiguous, result);

                }
                else if (MobileNo == null && mailId != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.Ambiguous);
                    result.Response.Message = "This Email already Register..";
                    _response = Request.CreateResponse(HttpStatusCode.Ambiguous, result);
                }
                else if (MobileNo != null && mailId != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.Ambiguous);
                    result.Response.Message = "This Email and Phone Number already Register..";
                    _response = Request.CreateResponse(HttpStatusCode.Ambiguous, result);
                }
                else
                {
                    int value = _objIUserSettings.UserSignUp(objSignUpRequestModel);

                    if (value > 0)
                    {
                        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                        result.Response.Message = "User register successfully....";
                        _response = Request.CreateResponse(HttpStatusCode.OK, result);

                    }
                    else
                    {
                        res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                        res.Response.Message = "Some Error Occurred...";
                        _response = Request.CreateResponse(HttpStatusCode.NotFound, res);

                    }
                }

            }
            catch (Exception ex)
            {
                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
            return _response;
        }

        [SecureResource]
        [HttpPost]
        [Route("EditUser")]
        public HttpResponseMessage EditUserById(EditUserProfileRequestModel objEditUserProfileRequestModel)
        {
            if (ModelState.IsValid)
            {
                VMResponse res = new VMResponse();
                EditUserProfileResponseModel result = new EditUserProfileResponseModel();
                try
                {
                    var headers = Request.Headers;
                    string token = headers.Authorization.Parameter.ToString();
                    Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                    result.Response = _objIUserSettings.EdituserProfile(objEditUserProfileRequestModel);
                    if (result.Response != null)
                    {
                        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                        result.Response.Message = "Success";
                        _response = Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.BadRequest);
                        res.Response.Message = "Bad Request...";
                        _response = Request.CreateResponse(HttpStatusCode.BadRequest, res);
                    }
                }
                catch (Exception ex)
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                    res.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Model is not valid");
            }
            return _response;
        }


        [SecureResource]
        [HttpPost]
        [Route("UpdateUser/{UserId}")]
        public HttpResponseMessage UpdateUserId(UpdateUserRequestModel objUpdateUserRequestModel, Int64 UserId)
        {

            VMResponse res = new VMResponse();
            try
            {
                int rowEffected = _objIUserSettings.UpdateUser(objUpdateUserRequestModel, UserId);
                if (rowEffected > 0)
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    res.Response.Message = "Your Profile is updated successfully";
                    _response = Request.CreateResponse(HttpStatusCode.OK, res);
                }
                else
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotAcceptable);
                    res.Response.Message = "Data which you are trying to update is incorrect Format.";
                    _response = Request.CreateResponse(HttpStatusCode.NotAcceptable, res);
                }
            }
            catch (Exception ex)
            {
                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = "Some error occured!!";
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }

            return _response;
        }



        [HttpPost]
        [SecureResource]
        [Route("UploadUserProfile")]
        public async Task<HttpResponseMessage> PostUserImage()
        {
            UploadUserPicRequestModel objUploadUserPicRequestModel = new UploadUserPicRequestModel();
            VMResponse res = new VMResponse();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                var httpRequest = HttpContext.Current.Request;
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 3; //Size = 3 MB 
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault().Substring(postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault().LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotAcceptable);
                            res.Response.Message = string.Format("Please Upload image of type .jpg,.gif,.png");
                            _response = Request.CreateResponse(HttpStatusCode.NotAcceptable, res);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotAcceptable);
                            res.Response.Message = string.Format("Please Upload a file upto 2 mb.");
                            _response = Request.CreateResponse(HttpStatusCode.NotAcceptable, res);
                        }
                        else
                        {
                            var filePath =HttpContext.Current.Server.MapPath("~/Content/ProfileImages/" + postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault());
                            objUploadUserPicRequestModel.ProfilePicture = _profileImagesPath + postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                            postedFile.SaveAs(filePath);
                            int value = _objHealthCareEntities.Database.ExecuteSqlCommand("UploadUserPicture @UserId=@UserId,@ProfilePicture=@ProfilePicture",
                            new SqlParameter("UserId", UserId),
                             new SqlParameter("ProfilePicture", objUploadUserPicRequestModel.ProfilePicture)
                           );
                            if (value > 0)
                            {
                                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                                res.Response.Message = "Profile Picture is updated successfully!!";
                                _response = Request.CreateResponse(HttpStatusCode.OK, res);
                            }
                            else
                            {
                                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotAcceptable);
                                res.Response.Message = "Picture which you are trying to update is incorrect Format.";
                                _response = Request.CreateResponse(HttpStatusCode.NotAcceptable, res);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = "Some error occured!!";
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
            return _response;
        }

        [HttpPost]
        [Route("GetUserImage")]
        public HttpResponseMessage GetUserImage()
        {
            UpdateUserPicResponseModel result = new UpdateUserPicResponseModel();
            VMResponse res = new VMResponse();
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                if (UserId != 0)
                {
                    string value = _objHealthCareEntities.Database.SqlQuery<string>("GetProfileImage @UserId=@UserId",
                             new SqlParameter("UserId", UserId)
                            ).FirstOrDefault();

                    if (value != null)
                    {
                        result.Response.ProfilePicture = CommonUrl.GetUrlAPI + value;
                        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                        result.Response.Message = "Success!!";
                        _response = Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                        res.Response.Message = "Image does not found!!";
                        _response = Request.CreateResponse(HttpStatusCode.NotFound, res);
                    }
                }
                else
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    res.Response.Message = "User Id does not found!!";
                    _response = Request.CreateResponse(HttpStatusCode.NotFound, res);
                }
            }
            catch (Exception ex)
            {

                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = "Some error occured!!";
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
            return _response;         
         }


        [HttpPost]
        [SecureResource]
        [Route("TestProfile")]
        public HttpResponseMessage ProfileUpload(UploadUserPicRequestModel objUploadUserPicRequestModel)
        {
            VMResponse res = new VMResponse();
            try
            {
                int maxSize = 6;
                var randonNo = GetUniqueKey(maxSize);

                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();


                byte[] imageBytes = Convert.FromBase64String(objUploadUserPicRequestModel.ProfilePicture);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                var dd = ConfigurationManager.AppSettings["IpRemote"];

                //string filepathfb = "http://18.219.253.188:8082";

                var pathx = "http://18.219.253.188:8082/Content/ProfileImages";
               // pathx = pathx.Replace("User", randonNo);
                string filename = "img" + randonNo;
                //Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image1 = System.Drawing.Image.FromStream(ms, true);
                image1.Save(System.Web.HttpContext.Current.Server.MapPath(pathx + filename), System.Drawing.Imaging.ImageFormat.Png);


                //image1.Save(pathx);

                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("UploadUserPicture @UserId=@UserId,@ProfilePicture=@ProfilePicture",
                            new SqlParameter("UserId", UserId),
                             new SqlParameter("ProfilePicture", pathx)
                           );
                if (value > 0)
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    res.Response.Message = "Profile Picture is updated successfully!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, res);
                }
                else
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotAcceptable);
                    res.Response.Message = "Picture which you are trying to update is incorrect Format.";
                    _response = Request.CreateResponse(HttpStatusCode.NotAcceptable, res);
                }
            }
            catch (Exception ex)
            {
                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = "Some error occured!!";
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);

            }
            return _response;
        }


        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }


        //protected void FTPUpload(object sender, EventArgs e)
        //{
        //    //FTP Server URL.
        //    string ftp = "ftp://yourserver.com/";

        //    //FTP Folder name. Leave blank if you want to upload to root folder.
        //    string ftpFolder = "Uploads/";

        //    byte[] fileBytes = null;

        //    //Read the FileName and convert it to Byte array.
        //    string fileName = Path.GetFileName(FileUpload1.FileName);
        //    using (StreamReader fileStream = new StreamReader(FileUpload1.PostedFile.InputStream))
        //    {
        //        fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
        //        fileStream.Close();
        //    }

        //    try
        //    {
        //        //Create FTP Request.
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp + ftpFolder + fileName);
        //        request.Method = WebRequestMethods.Ftp.UploadFile;

        //        //Enter FTP Server credentials.
        //        request.Credentials = new NetworkCredential("UserName", "Password");
        //        request.ContentLength = fileBytes.Length;
        //        request.UsePassive = true;
        //        request.UseBinary = true;
        //        request.ServicePoint.ConnectionLimit = fileBytes.Length;
        //        request.EnableSsl = false;

        //        using (Stream requestStream = request.GetRequestStream())
        //        {
        //            requestStream.Write(fileBytes, 0, fileBytes.Length);
        //            requestStream.Close();
        //        }

        //        FtpWebResponse response = (FtpWebResponse)request.GetResponse();

        //        lblMessage.Text += fileName + " uploaded.<br />";
        //        response.Close();
        //    }
        //    catch (WebException ex)
        //    {
        //        throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
        //    }
        //}
    }

}
