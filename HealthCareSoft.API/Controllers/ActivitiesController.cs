﻿using HealthCareSoft.API.Filter;
using HealthCareSoft.Entity;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using HealthCareSoft.Entity.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HealthCareSoft.API.Controllers
{
    [RoutePrefix("api/Activities")]
    public class ActivitiesController : ApiController
    {

        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        private IActivitiesRepository _objIActivitiesRepository;
        private HttpResponseMessage _response;

        public ActivitiesController()
        {
            _objIActivitiesRepository = new ActivitiesRepository();
        }

        [SecureResource]
        [HttpPost]
        [Route("ActivitiesList")]
        public HttpResponseMessage ActivitiesList()
        {
            ActivityListResponseModel result = new ActivityListResponseModel();
           
            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response.UpcomingAppointment = _objIActivitiesRepository.GetUpcommingActivitiesListByUserId(UserId).ToList();
                result.Response.ReportList = _objIActivitiesRepository.ReportList(UserId).ToList();
                result.Response.PillsReminder = _objIActivitiesRepository.PillsReminderList(UserId);


                if (result.Response.PillsReminder != null && result.Response.ReportList.Count > 0 && result.Response.UpcomingAppointment.Count > 0)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success...";
                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NoContent);
                    result.Response.Message = "Not Record Found...";
                }
                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }
    }
}
