﻿using HealthCareSoft.API.Filter;
using HealthCareSoft.API.Models;
using HealthCareSoft.Entity;
using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using HealthCareSoft.Entity.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;



namespace HealthCareSoft.API.Controllers
{
    [RoutePrefix("api/Patient")]
    public class PatientController : ApiController
    {

        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        private HttpResponseMessage _response;
        private IPatientRepository _objIPatientRepository;
        public PatientController()
        {
            _objIPatientRepository = new PatientRepository();
        }


        //[HttpPost]
        //[Route("ReportDetailById")]
        //[SecureResource]
        //public HttpResponseMessage ReportDetailsById(ReportDetailByIdRequestModel objReportDetailByIdRequestModel)
        //{
        //    ReportDetailByIdResponseModel result = new ReportDetailByIdResponseModel();
        //    try
        //    {
        //        var headers = Request.Headers;
        //        string token = headers.Authorization.Parameter.ToString();
        //        Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

        //        result.Response = _objIPatientRepository.GetReportDetailById(objReportDetailByIdRequestModel);


        //        if (result != null)
        //        {
        //            result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
        //            result.Response.Message = "Success!!";
        //            _response = Request.CreateResponse(HttpStatusCode.OK, result);

        //        }
        //        else
        //        {
        //            result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
        //            result.Response.Message = "Some Error Occurred...";

        //            _response = Request.CreateResponse(HttpStatusCode.OK, result);

        //        }
        //        _response = Request.CreateResponse(HttpStatusCode.OK, result);
        //    }
        //    catch (Exception Ex)
        //    {
        //        result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
        //        result.Response.Message = Ex.ToString();
        //        _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

        //    }
        //    return _response;
        //}


        [SecureResource]
        [HttpPost]
        [Route("ReportList")]
        public HttpResponseMessage ReportList(ReportListRequestModel objReportListRequestModel)
        {
            ReportListResponseModel result = new ReportListResponseModel();

            try
            {
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response.ReportListPatient = _objIPatientRepository.GetReportList(objReportListRequestModel).ToList();

                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }


                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }


        [SecureResource]
        [HttpPost]
        [Route("PrescriptionList")]
        public HttpResponseMessage PrescriptionList(PrescriptionListRequestModel objPrescriptionListRequestModel)
        {
            PrescriptionListModel result = new PrescriptionListModel();

            try
            {
                var headers = Request.Headers;

                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result = _objIPatientRepository.GetPrescriptionList(objPrescriptionListRequestModel, UserId);


                foreach (var item in result.Response.PrescriptionList)
                {


                    if (item.Status == "Open")
                    {

                        //item.StatusImage = "http://180.151.232.92:91/StatusImage/close.png";
                        //item.ProfilePicture = "http://180.151.232.92:94" + item.ProfilePicture;
                        item.StatusImage = "http://18.219.253.188:8081/StatusImage/close.png";
                        item.ProfilePicture = CommonUrl.GetUrlAPI + item.ProfilePicture;
                        if (item.ProfilePicture == null)
                        {
                            item.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";

                        }


                    }
                    else
                    {

                        item.StatusImage = "http://18.219.253.188:8081/StatusImage/close.png";
                        item.ProfilePicture = CommonUrl.GetUrlAPI + item.ProfilePicture;
                        if (item.ProfilePicture == null)
                        {
                            item.ProfilePicture = "http://18.219.253.188:8082/Content/ProfileImages/img.jpg";

                        }
                    }
                }



                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                result.Response.Message = "Success...";

                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }



        [SecureResource]
        [HttpPost]
        [Route("GetDeparment")]
        public HttpResponseMessage GetDeparmentList()
        {
            DepartmentResponseModel result = new DepartmentResponseModel();
            try
            {

                var headers = Request.Headers;

                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response.DepartmentList = _objIPatientRepository.GetDepartment().ToList();
                //List<DepartmentList> obj = _objHealthCareEntities.Database.SqlQuery<DepartmentList>("select Id,Name from Departments").ToList();
                if (result.Response.DepartmentList != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }

            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }

        [SecureResource]
        [HttpPost]
        [Route("GetDoctorListByDepartment")]
        public HttpResponseMessage GetDoctorListByDepartment(DoctorByDepartmentRequestModel model)
        {
            DoctorByDepartmentResponseModel result = new DoctorByDepartmentResponseModel();


            try
            {

                var headers = Request.Headers;

                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response.DoctorByDepartmentList = _objIPatientRepository.GetDoctorByDept(model.DepartmentId).ToList();

                foreach (var item in result.Response.DoctorByDepartmentList)
                {
                    item.ProfilePicture = CommonUrl.GetUrlAPI + item.ProfilePicture;
                }

                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }

            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);

            }
            return _response;
        }



        [HttpPost]
        [Route("GetReasonForVisit")]
        public HttpResponseMessage GetReasonForVisitList()
        {
            ReasonforvisitResponseModel result = new ReasonforvisitResponseModel();
            try
            {
                result.Response.ReasonList = _objIPatientRepository.GetReasonForVisitList().ToList();
                if (result != null)
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    result.Response.Message = "Some Error Occurred...";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                result.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return _response;
        }



        //[SecureResource]
        //[HttpPost]
        //[Route("ReportByUserId")]
        //public HttpResponseMessage ReportByUserId(ReportListRequestModel objReportListRequestModel)
        //{
        //    ReportModelResponse result = new ReportModelResponse();
        //    VMResponse res = new VMResponse();
        //    try
        //    {
        //        var headers = Request.Headers;
        //        string token = headers.Authorization.Parameter.ToString();
        //        Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

        //        //result.Response = _objIPatientRepository.GetReportById(objReportListRequestModel);
        //        byte[] contents = (byte[])result.Response.Data;

               
        //        var mappedPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Tempfiles/Test.pdf");
        //        mappedPath = mappedPath.Replace("Test", UserId.ToString());
        //        System.IO.File.WriteAllBytes(mappedPath, contents);

        //        var path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Tempfiles/Test.pdf";
        //        path = path.Replace("Test", UserId.ToString());

        //        result.Response.Data = null;

        //        if (result != null)
        //        {     
        //            result.Response.PdfUrl = path;
        //            result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
        //            result.Response.Message = "Success!!";
        //            _response = Request.CreateResponse(HttpStatusCode.OK, result);
        //        }
        //        else
        //        {
        //            res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
        //            res.Response.Message = "Report Does not exist...";
        //            _response = Request.CreateResponse(HttpStatusCode.NotFound, res);
        //        }
        //        _response = Request.CreateResponse(HttpStatusCode.OK, result);
        //    }
        //    catch (Exception ex)
        //    {
        //        res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
        //        res.Response.Message = ex.ToString();
        //        _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);

        //    }
        //    return _response;
        //}

        [SecureResource]
        [HttpPost]
        [Route("ReportListByUserIdUpdated")]
        public HttpResponseMessage ReportByUserIdUpdated(ReportListRequestModel objReportListRequestModel)
        {
            string path=string.Empty;
            ReportModelResponse result = new ReportModelResponse();
            VMResponse res = new VMResponse();
            try
            {
                byte[] contents = null;
                var headers = Request.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 UserId = _objHealthCareEntities.Database.SqlQuery<Int64>("select userId from Token where TokenCode={0}", token).FirstOrDefault();

                result.Response.ReportList = _objIPatientRepository.GetReportById(objReportListRequestModel);
                if(result.Response.ReportList.Count>0)
                {
                   foreach(var item2 in result.Response.ReportList)
                    {
                        if(item2.ReportTitle ==1)
                        {
                            item2.ReportName = "Blood Report";
                        }
                        else
                        {
                            item2.ReportName = "X-Ray Report";
                        }
                    }
                }
                foreach (var item in result.Response.ReportList)
                {
                    contents = (byte[])item.Data;

                    var mappedPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Tempfiles/Test.pdf");
                    mappedPath = mappedPath.Replace("Test", UserId.ToString());
                    System.IO.File.WriteAllBytes(mappedPath, contents);

                     path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Tempfiles/Test.pdf";
                    path = path.Replace("Test", UserId.ToString());
                }

                if (result != null)
                {
                    foreach(var item1 in result.Response.ReportList)
                    {
                        item1.ReportUrl = path;
                    }
                    //result.Response.ReportList = path;
                    result.Response.StatusCode = Convert.ToInt32(HttpStatusCode.OK);
                    result.Response.Message = "Success!!";
                    _response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.NotFound);
                    res.Response.Message = "Report Does not exist...";
                    _response = Request.CreateResponse(HttpStatusCode.NotFound, res);
                }
                _response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                res.Response.StatusCode = Convert.ToInt32(HttpStatusCode.InternalServerError);
                res.Response.Message = ex.ToString();
                _response = Request.CreateResponse(HttpStatusCode.InternalServerError, res);

            }
            return _response;
        }
    }
}
