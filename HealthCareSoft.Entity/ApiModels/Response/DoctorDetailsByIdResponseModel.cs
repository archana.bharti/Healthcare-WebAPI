﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class DoctorDetailsByIdResponseModel
    {
        public DoctorDetailsResponse Response { get; set; }
    }
    public class Monday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class Tuesday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class Wednesday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class Thrusday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class Friday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class Saturday
    {
        public string Time { get; set; }
        public string IsAvail { get; set; }
    }

    public class AvalibiltyList
    {
        public Monday Monday { get; set; }
        public Tuesday Tuesday { get; set; }
        public Wednesday Wednesday { get; set; }
        public Thrusday Thrusday { get; set; }
        public Friday Friday { get; set; }
        public Saturday Saturday { get; set; }
    }

    public class DoctorDetailsResponse
    {
        public DoctorDetailsResponse()
        {
            AvalibiltyList = new List<AvalibiltyList>();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<AvalibiltyList> AvalibiltyList { get; set; }
    }

}
