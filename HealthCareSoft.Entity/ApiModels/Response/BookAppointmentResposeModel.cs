﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class BookAppointmentResposeModel
    {
        public BookAppointmentResposeModel()
        {
            Response = new BookAppointmentResponse();
        }
        public BookAppointmentResponse Response { get; set; }
    }

    public class BookAppointmentResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public Int64 Id { get; set; }
    }
}
