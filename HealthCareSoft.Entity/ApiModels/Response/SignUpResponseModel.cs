﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class SignUpResponseModel
    {
        public SignUpResponseModel()
        {
            Response = new UserSignUpResponse();
        }
        public UserSignUpResponse Response { get; set; }

    }

    public class UserSignUpResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
