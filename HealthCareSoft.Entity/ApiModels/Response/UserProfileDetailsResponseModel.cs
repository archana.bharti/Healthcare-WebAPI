﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
   public class UserProfileDetailsResponseModel
    {
        public UserProfileDetailsResponseModel()
        {
            Response = new UserDetailResponse();
        }
        public UserDetailResponse Response { get; set; }
    }

    public class UserDetailResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public Int64 UserId { get; set; }
        public string ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string PhoneNo { get; set; }
        public string DateOfBirth { get; set; }
        public int UserPin { get; set; }
    }
}
