﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
   public class UpdateUserPicResponseModel
    {
        public UpdateUserPicResponseModel()
            {
            Response = new UpdatePicResponse();
            }
        public UpdatePicResponse Response  { get; set; }
    }
    public class UpdatePicResponse
    {
        public string ProfilePicture { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}
