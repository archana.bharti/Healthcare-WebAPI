﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
   public class ReportModelResponse
    {
       
        public ReportModelResponse()
        {
            Response = new ReportListUpResponse();
        }
        public ReportListUpResponse Response { get; set; }
    }
    public class ReportResponse
    {
        public Int64 ReportId { get; set; }
    
        public int ReportTitle { get; set; }
        public byte[] Data { get; set; }
        public string FileName { get; set; }
        public string ReportUrl { get; set; }
        public string AddedBy { get; set; }
        public string ReportDateTime { get; set; }
        public string ReportTime { get; set; }
        public string ReportName { get; set; }
       
    }
    public class ReportListUpResponse
    {
        public ReportListUpResponse()
        {
            ReportList = new List<ReportResponse>();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<ReportResponse> ReportList { get; set; }
    }
}
