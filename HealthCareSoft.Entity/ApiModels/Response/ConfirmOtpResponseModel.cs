﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class ConfirmOtpResponseModel
    {
        public ConfirmOtpResponseModel()
        {
            Response = new confirmOtpResponse();
        }
        public confirmOtpResponse Response { get; set; }
    }

    public class confirmOtpResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string PhoneNo { get; set; }
        public DateTime? DOB { get; set; }
       
        public string ProfilePicture { get; set; }
        public int? UserPin { get; set; }
        public string Address { get; set; }
        public string TokenCode { get; set; }
        public Int64 Id { get; set; }
        public int RoleId { get; set; }




    }
}
