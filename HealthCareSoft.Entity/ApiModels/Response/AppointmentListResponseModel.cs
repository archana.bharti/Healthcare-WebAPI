﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class AppointmentListResponseModel
    {
        public AppointmentListResponseModel()
        {
            Response = new AppointmentListResponse();
        }
        public AppointmentListResponse Response { get; set; }
    }

    public class AppointmentList
    {
        public Int64 AppointmentId { get; set; }
        public string DoctorName { get; set; }
        //public string BookId { get; set; }
        public string PreviousDate { get; set; }
        public string NextDate { get; set; }
        public string DoctorProfilePic { get; set; }
        public int QueueNumber { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string QualificationName { get; set; }
        public string ReasonForVisit { get; set; }
        public string HospitalName { get; set; }
    }

    public class AppointmentListResponse
    {
        public AppointmentListResponse()
        {
            AppointmentList = new List<AppointmentList>(); ;
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<AppointmentList> AppointmentList { get; set; }
    }
}
