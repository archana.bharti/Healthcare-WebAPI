﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class VMResponse
    {
       public VMResponse()
        {
            Response = new VMResponseModel();
        }
        public VMResponseModel Response { get; set; }
    }
    public class VMResponseModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
