﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class UserEditProfileResponseModel
    {
        public UserEditProfileResponseModel()
        {
            Response = new UserEditResponse();

        }
        public UserEditResponse Response { get; set; }
    }
    public class UserEditResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
