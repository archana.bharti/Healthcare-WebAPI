﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class LoginResponseModel
    {
        public LoginResponseModel()
        {
            Response = new LoginResponse();
        }
        public LoginResponse Response { get; set; }
    }

    public class LoginResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public long UserId { get; set; }
    }
}
