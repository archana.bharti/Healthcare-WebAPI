﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class ActivityListResponseModel
    {
        public ActivityListResponseModel()
        {
            Response = new ActivityListResponse();
        }
        public ActivityListResponse Response { get; set; }
    }

    public class UpcomingAppointment
    {
        public Int64 Id { get; set; }
        public DateTime AppointmentDate { get; set; }
        // public virtual TimeSpan AppointmentTime { get; private set; }
        public string Time { get; set; }

        public string ReasonForVisit { get; set; }
        public string HospitalName { get; set; }
    }

    public class ReportList
    {
        public Int64 ReportId { get; set; }
        public DateTime ReportDateTime { get; set; }
        public string ReportTitle { get; set; }
        public string AddedBy { get; set; }
    }

    public class Tablet
    {
        public string TabletName { get; set; }
        public string Repetation { get; set; }
    }

    public class PillsReminder
    {
        public PillsReminder()
        {
            Tablet = new List<Tablet>();
        }
        public Int64 ReminderId { get; set; }
        public string ReminderTime { get; set; }
        public string PillsFordiseases { get; set; }
        public string HospitalName { get; set; }
        public List<Tablet> Tablet { get; set; }
    }

    public class ActivityListResponse
    {
        public ActivityListResponse()
        {
            UpcomingAppointment = new List<UpcomingAppointment>();
            PillsReminder = new PillsReminder();
            ReportList = new List<ReportList>();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<UpcomingAppointment> UpcomingAppointment { get; set; }
        public List<ReportList> ReportList { get; set; }
        public PillsReminder PillsReminder { get; set; }
    }
}
