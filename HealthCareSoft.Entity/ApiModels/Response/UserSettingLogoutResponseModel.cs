﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class UserSettingLogoutResponseModel
    {
        public UserSettingLogoutResponseModel()
        {
            Response = new UserLogoutResponse();
        }
        public UserLogoutResponse Response { get; set; }
    }

    public class UserLogoutResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

}
