﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class ReportListResponseModel
    {
        public ReportListResponseModel()
        {
            Response = new ReportListResponse();

        }
        public ReportListResponse Response { get; set; }
    }
    public class ReportListPatient
    {
        public Int64 ReportId { get; set; }
        public string ReportTitle { get; set; }
        public string AddedBy { get; set; }
       // public DateTime ReportDateTime { get; set; }
        public string ReportURL { get; set; }
        public string ReportDateTime { get; set; }
        public string ReportTime { get; set; }
    }

    public class ReportListResponse
    {
        public ReportListResponse()
        {
            ReportListPatient = new List<ReportListPatient>();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<ReportListPatient> ReportListPatient { get; set; }
    }
}
