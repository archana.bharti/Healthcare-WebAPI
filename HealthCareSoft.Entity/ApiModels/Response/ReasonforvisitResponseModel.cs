﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class ReasonforvisitResponseModel
    {
        public ReasonforvisitResponseModel()
        {
            Response = new ReasonforvisitResponse();
        }

        public ReasonforvisitResponse Response { get; set; }
    }
    public class ReasonforvisitList
    {
        public Int64 Id { get; set; }
        public string ReasonForVisit { get; set; }
    }
    public class ReasonforvisitResponse
    {
       public List<ReasonforvisitList> ReasonList { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
