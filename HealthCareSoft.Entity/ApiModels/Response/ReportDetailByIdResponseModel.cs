﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
   public class ReportDetailByIdResponseModel
    {
        public ReportDetailByIdResponseModel()
        {
            Response = new ReportDetailByIdResponse();
        }
        public ReportDetailByIdResponse Response { get; set; }

    }

    public class ReportDetailByIdResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string ReportUrl { get; set; }
    }
}
