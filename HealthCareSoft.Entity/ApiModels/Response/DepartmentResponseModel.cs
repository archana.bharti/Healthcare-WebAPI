﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class DepartmentResponseModel
    {
        public DepartmentResponseModel()
        {
            Response = new DepartmentResponse();
        }
        public DepartmentResponse Response { get; set; }
    }
    public class DepartmentList
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
      
    }
    public class DepartmentResponse
    {
        public List<DepartmentList> DepartmentList { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

}
