﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class DoctorByDepartmentResponseModel
    {
        public DoctorByDepartmentResponseModel()
        {
            Response = new DoctorByDepartmentResponse();
        }
        public DoctorByDepartmentResponse Response { get; set; }
    }

    public class DoctorByDepartmentList
    {
        public Int64 Id { get; set; }
        public string DoctorName { get; set; }
        public string DepartmentName { get; set; }
        public string HospitalName { get; set; }
        public string ProfilePicture { get; set; }
        public string QualificationName { get; set; }

    }
    public class DoctorByDepartmentResponse
    {
        public List<DoctorByDepartmentList> DoctorByDepartmentList { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
