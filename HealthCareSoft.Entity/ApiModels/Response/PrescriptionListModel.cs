﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Response
{
    public class PrescriptionListModel
    {
        public PrescriptionListModel()
        {
            Response = new Response();
        }
        public Response Response { get; set; }
    }

    public class TabletList
    {
        public string TabletName { get; set; }
        public string Repetaion { get; set; }
        public string Power { get; set; }
    }

    public class PrescriptionList
    {
        public PrescriptionList()
        {
            TabletList = new List<TabletList>();
        }
        public Int64? PrescriptionId { get; set; }
        public string DoctorName { get; set; }
        public string HospitalName { get; set; }
        public string Status { get; set; }
        public string ProfilePicture { get; set; }
        public string StatusImage { get; set; }
        public string CreatedOn { get; set; }
        public string PrescriptionTime { get; set; }
        public List<TabletList> TabletList { get; set; }
    }

    public class Response
    {
        public Response()
        {
            PrescriptionList = new List<PrescriptionList>();
        }
        public List<PrescriptionList> PrescriptionList { get; set; }
        public Int32? StatusCode { get; set; }
        public String Message { get; set; }
    }
}
