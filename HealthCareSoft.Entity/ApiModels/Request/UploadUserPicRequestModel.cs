﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
   public class UploadUserPicRequestModel
    {
        public string ProfilePicture { get; set; }
    }
}
