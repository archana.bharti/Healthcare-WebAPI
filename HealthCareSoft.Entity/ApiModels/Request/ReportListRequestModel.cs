﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class ReportListRequestModel
    {
        public Int64 UserId { get; set; }
        public Boolean IsOrderByDate { get; set; }
        public Boolean OrderByName { get; set; }
        public string DoctorName { get; set; }
        public string ClinicName { get; set; }
        public int Limit { get; set; }
        public int OffSet { get; set; }
    }
}
