﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class UserProfileDetailsRequestModel
    {
        public Int64 UserId { get; set; }
    }
}
