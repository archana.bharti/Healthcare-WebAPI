﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class ConfirmOtpRequestModel
    {
        public Int64 Otp { get; set; }
        public Int64 UserId { get; set; }
    }
}
