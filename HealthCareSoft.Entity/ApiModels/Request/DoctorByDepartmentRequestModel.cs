﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class DoctorByDepartmentRequestModel
    {
        public Int64 DepartmentId { get; set; }
    }
}
