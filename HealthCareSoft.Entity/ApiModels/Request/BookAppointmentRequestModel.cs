﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class BookAppointmentRequestModel
    {
        public Int64 AppointmentId { get; set; }
        public string ReasonForVisit { get; set; }
        public DateTime AppointmentDate { get; set; }
        public DateTime AppointmentTime { get; set; }
       // public Int64 DiseaseId { get; set; }
        public Int64 DoctorId { get; set; }
        public Int64 PatientId { get; set; }

        public Int64 UserId { get; set; }
        public string Illness { get; set; }

    }
}
