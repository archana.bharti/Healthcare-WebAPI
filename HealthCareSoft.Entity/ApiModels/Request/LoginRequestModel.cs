﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
  public  class LoginRequestModel
    {
        public string PhoneNo { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
    }
}
