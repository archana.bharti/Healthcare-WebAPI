﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareSoft.API.Models
{
    public static class CommonUrl
    {
        public static string GetUrlAPI
        {
             get
            {
                return "http://18.219.253.188:8081";
            }
        }
        public static string GetUrlAdmin
        {
            get
            {
                return "http://18.219.253.188:8082";
            }
        }
    }
}