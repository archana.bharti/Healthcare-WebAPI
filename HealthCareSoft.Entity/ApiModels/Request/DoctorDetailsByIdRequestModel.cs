﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class DoctorDetailsByIdRequestModel
    {
        public string AvailibiltyFrom { get; set; }
        public string AvailibiltyTo { get; set; }
        public string DoctorId { get; set; }
    }
}                                                                       
