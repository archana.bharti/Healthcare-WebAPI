﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class AppointmentListRequestModel
    {
        public Int64 UserId { get; set; }
        public int ListType { get; set; }
        public string Key { get; set; }
      

    }
}
