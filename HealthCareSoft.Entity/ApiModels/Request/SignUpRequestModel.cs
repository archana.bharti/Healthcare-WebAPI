﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.ApiModels.Request
{
    public class SignUpRequestModel
    {

       public string FirstName { get; set; }
       public string MiddelName { get; set; }
       public string LastName { get; set; }
       public string Address { get; set; }
      public string PostalCode { get; set; }
      public string PhoneNo { get; set; }
      public string Email { get; set; }
      public string Password { get; set; }
      public string Gender { get; set; }
   
     public DateTime DOB { get; set; }
   

    }
}
