﻿using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.IRepository
{
    public interface IPatientRepository
    {
        ReportDetailByIdResponse GetReportDetailById(ReportDetailByIdRequestModel objReportDetailByIdRequestModel);
        List<ReportListPatient> GetReportList(ReportListRequestModel objReportListRequestModel);
        PrescriptionListModel GetPrescriptionList(PrescriptionListRequestModel objPrescriptionListRequestModel,long UserId);
        List<DepartmentList> GetDepartment();
        List<DoctorByDepartmentList> GetDoctorByDept(Int64 DepartmentId);
        List<ReasonforvisitList> GetReasonForVisitList();
        List<ReportResponse> GetReportById(ReportListRequestModel objReportListRequestModel);
    }
}
