﻿using HealthCareSoft.Entity.ApiModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.IRepository
{
    public interface IActivitiesRepository
    {

        List<UpcomingAppointment> GetUpcommingActivitiesListByUserId(long UserId);
        List<ReportList> ReportList(long UserId);
        HealthCareSoft.Entity.ApiModels.Response.PillsReminder PillsReminderList(long UserId);
    }
}
