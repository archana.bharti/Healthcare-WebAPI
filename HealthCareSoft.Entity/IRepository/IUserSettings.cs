﻿using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.IRepository
{
 public interface IUserSettings
    {
        UserProfileModel Login(LoginRequestModel objLoginRequestModel);
        int UpdateOtp(long otp, string PhoneNo);
        confirmOtpResponse ConfirmOtp(ConfirmOtpRequestModel objConfirmOtpRequestModel);
        UserDetailResponse GetUserDetails(Int64 UserId);
        int EditProfileDetail(UserEditProfileRequestModel objUserEditProfileRequestModel);
        int LogoutUser(Int64 UserId);
        int UserSignUp(SignUpRequestModel objSignUpRequestModel);
        EditUserProfile EdituserProfile(EditUserProfileRequestModel objEditUserProfileRequestModel);
        int UpdateUser(UpdateUserRequestModel objUpdateUserRequestModel,Int64 UserId);
    }   
}
