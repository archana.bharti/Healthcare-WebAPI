﻿using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.IRepository
{
    public interface IAppoinmentRepository
    {
        int BookAppoinment(BookAppointmentRequestModel objBookAppointmentRequestModel);
        List<AppointmentList> GetAppointmentListOfPatient(Int64 UserId, int ListType, string Key);
    }
}
