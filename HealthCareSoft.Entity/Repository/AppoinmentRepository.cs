﻿using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.Repository
{
    public class AppoinmentRepository: IAppoinmentRepository
    {
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        public int BookAppoinment(BookAppointmentRequestModel objBookAppointmentRequestModel)
        {
            
            try
            {
                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("SaveBookAppoinment @ReasonForVisit=@ReasonForVisit,@AppointmentDate=@AppointmentDate,@AppointmentTime=@AppointmentTime,@DoctorId=@DoctorId,@PatientId=@PatientId,@Illness=@Illness",
                             new SqlParameter("ReasonForVisit", objBookAppointmentRequestModel.ReasonForVisit),
                             new SqlParameter("AppointmentDate", objBookAppointmentRequestModel.AppointmentDate),
                             new SqlParameter("AppointmentTime", objBookAppointmentRequestModel.AppointmentTime),
                             new SqlParameter("DoctorId", objBookAppointmentRequestModel.DoctorId),
                             new SqlParameter("PatientId", objBookAppointmentRequestModel.PatientId),
                             new SqlParameter("CreatedBy", objBookAppointmentRequestModel.PatientId),
                             new SqlParameter("Illness", objBookAppointmentRequestModel.Illness));
                
                return value;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public List<AppointmentList> GetAppointmentListOfPatient(Int64 UserId,int ListType,string Key)
        {
           
            try
            {
                List<AppointmentList> objAppointmentList = _objHealthCareEntities.Database.SqlQuery<AppointmentList>("GetAppointmentList @UserId=@UserId,@Type=@Type,@Key=@Key",
                                                           new SqlParameter("UserId", UserId),
                                                           new SqlParameter("Type", ListType),
                                                           new SqlParameter("Key", Key)).ToList();
                return objAppointmentList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
    

}
