﻿using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.Repository
{
    public class ActivitiesRepository: IActivitiesRepository
    {
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        public List<UpcomingAppointment> GetUpcommingActivitiesListByUserId(long UserId)
        {
            try
            {
                List<UpcomingAppointment> objUpcomingAppointment = _objHealthCareEntities.Database.SqlQuery<UpcomingAppointment>("GetUpcommingActivitiesListByUserId @UserId=@UserId",
                                                             new SqlParameter("UserId", UserId)).ToList();

                return objUpcomingAppointment;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ReportList> ReportList(long UserId)
        {
            try
            {
                List<ReportList> objReportList = _objHealthCareEntities.Database.SqlQuery<ReportList>("GetReportListByUserId @UserId=@UserId",
                                                             new SqlParameter("UserId", UserId)).ToList();

                return objReportList;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
        public HealthCareSoft.Entity.ApiModels.Response.PillsReminder PillsReminderList(long UserId)
        {
            ActivityListResponseModel objActivityListResponseModel = new ActivityListResponseModel();
            try
            {
                HealthCareSoft.Entity.ApiModels.Response.PillsReminder objPillsReminder = _objHealthCareEntities.Database.SqlQuery<HealthCareSoft.Entity.ApiModels.Response.PillsReminder>("GetPillsReminderByUserId @UserId=@UserId",
                                                             new SqlParameter("UserId", UserId)).FirstOrDefault();

                foreach (var item in objPillsReminder.Tablet)
                {
                    List<Tablet> objTabletList = _objHealthCareEntities.Database.SqlQuery<Tablet>("GetTabletList @UserId=@UserId",
                                                 new SqlParameter("UserId", UserId)).ToList();
                    //item.Tablet = objTabletList;
                }
                //for (int i = 0; i < objPillsReminder.Count; i++)
                //{
                //    List<Tablet> objTabletList = _objHealthCareEntities.Database.SqlQuery<Tablet>("GetTabletList @UserId=@UserId",
                //                               new SqlParameter("UserId", UserId)).ToList();

                //}
                return objPillsReminder;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

    }
}
