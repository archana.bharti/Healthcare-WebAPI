﻿using HealthCareSoft.API.Models;
using HealthCareSoft.Entity.ApiModels;
using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.Repository
{
    public class UserSettings : IUserSettings
    {
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();
        public UserProfileModel Login(LoginRequestModel objLoginRequestModel)
        {
            try
            {
                UserProfileModel objUserProfile = _objHealthCareEntities.Database.SqlQuery<UserProfileModel>("Login @PhoneNo=@PhoneNo,@DeviceId=@DeviceId,@DeviceType=@DeviceType",
                                      new SqlParameter("PhoneNo", objLoginRequestModel.PhoneNo),
                                      new SqlParameter("DeviceId", objLoginRequestModel.DeviceId),
                                      new SqlParameter("DeviceType", objLoginRequestModel.DeviceType)
                                     ).FirstOrDefault();

                return objUserProfile;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public  int UpdateOtp(long otp,string PhoneNo)
        {
            try
            {
                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("UpdateOtp @otp=@otp,@PhoneNo=@PhoneNo",
                                      new SqlParameter("otp", otp),
                                      new SqlParameter("PhoneNo",PhoneNo));
                                    
                                    

                return value;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public confirmOtpResponse ConfirmOtp(ConfirmOtpRequestModel objConfirmOtpRequestModel)
        {
            try
            {
                confirmOtpResponse objconfirmOtpResponse = _objHealthCareEntities.Database.SqlQuery<confirmOtpResponse>("ConfirmOtp @Otp=@Otp,@UserId=@UserId",
                                     new SqlParameter("Otp", objConfirmOtpRequestModel.Otp),
                                     new SqlParameter("UserId", objConfirmOtpRequestModel.UserId)).FirstOrDefault();

                return objconfirmOtpResponse;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public UserDetailResponse GetUserDetails(Int64 UserId)
        {
            try
            {
                UserDetailResponse objUserDetailResponse = _objHealthCareEntities.Database.SqlQuery<UserDetailResponse>("GetUserProfileDetailById @UserId=@UserId",
                                                            new SqlParameter("UserId", UserId)).FirstOrDefault();
                return objUserDetailResponse;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public int EditProfileDetail(UserEditProfileRequestModel objUserEditProfileRequestModel)
        {
            
            try
            {


                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("EditUserprofile @UserId=@UserId,@UserPin=@UserPin",
                                                      new SqlParameter("UserId", objUserEditProfileRequestModel.UserId),
                                                       new SqlParameter("UserPin", objUserEditProfileRequestModel.UserPin));
                return value;
            }
            catch(Exception ex)
            {
                return 0;
            }
                
        }

        public int LogoutUser(Int64 UserId)
        {
            try
            {
                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("Logout @UserId=@UserId",
                            new SqlParameter("UserId", UserId));
                return value;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public List<AvalibiltyList> GetDoctorDetailById()
        {
            try
            {

                List<AvalibiltyList> objAvalibiltyList = _objHealthCareEntities.Database.SqlQuery<AvalibiltyList>("GetDoctorDetailById").ToList();

                return objAvalibiltyList;
            }
            catch(Exception ex)
            {
                return null;

            }
        }

        public int UserSignUp(SignUpRequestModel objSignUpRequestModel)
        {
            try
            {
                objSignUpRequestModel.Password = CryptorEngine.Encrypt(objSignUpRequestModel.Password, true);

                int value = _objHealthCareEntities.Database.ExecuteSqlCommand("UserSignUp @FirstName=@FirstName,@MiddelName=@MiddelName,@LastName=@LastName,@Address=@Address,@PostalCode=@PostalCode,@PhoneNo=@PhoneNo,@Email=@Email,@Password=@Password,@Gender=@Gender,@DOB=@DOB",
                         
                                                                                new SqlParameter("FirstName", objSignUpRequestModel.FirstName),
                                                                                new SqlParameter("MiddelName", (object)objSignUpRequestModel.MiddelName ?? DBNull.Value),
                                                                                new SqlParameter("LastName", objSignUpRequestModel.LastName),
                                                                                new SqlParameter("Address", objSignUpRequestModel.Address),
                                                                                new SqlParameter("PostalCode", objSignUpRequestModel.PostalCode),
                                                                                new SqlParameter("PhoneNo", objSignUpRequestModel.PhoneNo),
                                                                                new SqlParameter("Email", objSignUpRequestModel.Email),
                                                                                new SqlParameter("Password", objSignUpRequestModel.Password),
                                                                                new SqlParameter("Gender", objSignUpRequestModel.Gender),                                                                              
                                                                                new SqlParameter("DOB", objSignUpRequestModel.DOB)
                                                                           );
                return value;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }


        public EditUserProfile EdituserProfile(EditUserProfileRequestModel objEditUserProfileRequestModel)
        {
            try
            {

                EditUserProfile model = _objHealthCareEntities.Database.SqlQuery<EditUserProfile>("EditUser @UserId=@userId",
                                                    new SqlParameter("UserId", objEditUserProfileRequestModel.UserId)).FirstOrDefault();
                model.Password = CryptorEngine.Decrypt(model.Password, true);

                model.ProfilePicture = CommonUrl.GetUrlAPI + model.ProfilePicture;
                return model;

            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public int UpdateUser(UpdateUserRequestModel objUpdateUserRequestModel,Int64 UserId)
        {
            try
            {
                objUpdateUserRequestModel.Password = CryptorEngine.Encrypt(objUpdateUserRequestModel.Password, true);
                int rowEffected = _objHealthCareEntities.Database.ExecuteSqlCommand("UpdateUserProfile @userId=@UserId, @FirstName=@FirstName,@MiddelName=@MiddelName,@LastName=@LastName,@Address=@Address,@PostalCode=@PostalCode,@PhoneNo = @PhoneNo, @Email = @Email, @Password = @Password, @Gender = @Gender, @DOB = @DOB",
                                        new SqlParameter("userId",UserId),
                                        new SqlParameter("FirstName", (object)objUpdateUserRequestModel.FirstName ?? DBNull.Value),
                                         new SqlParameter("MiddelName", (object)objUpdateUserRequestModel.MiddelName ?? DBNull.Value),
                                         new SqlParameter("LastName", (object)objUpdateUserRequestModel.LastName ?? DBNull.Value),
                                         new SqlParameter("Address", (object)objUpdateUserRequestModel.Address ?? DBNull.Value),
                                         new SqlParameter("PostalCode", (object)objUpdateUserRequestModel.PostalCode ?? DBNull.Value),
                                         new SqlParameter("PhoneNo", (object)objUpdateUserRequestModel.PhoneNo ?? DBNull.Value),
                                         new SqlParameter("Email", (object)objUpdateUserRequestModel.Email ?? DBNull.Value),
                                         new SqlParameter("Password", (object)objUpdateUserRequestModel.Password ?? DBNull.Value),
                                         new SqlParameter("Gender", (object)objUpdateUserRequestModel.Gender ?? DBNull.Value),                                         
                                         new SqlParameter("DOB", (object)objUpdateUserRequestModel.DOB ?? DBNull.Value));
                return rowEffected;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }
    }
}