﻿using HealthCareSoft.Entity.ApiModels.Request;
using HealthCareSoft.Entity.ApiModels.Response;
using HealthCareSoft.Entity.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareSoft.Entity.Repository
{
    public class PatientRepository: IPatientRepository
    {
        private HealthCareEntities _objHealthCareEntities = new HealthCareEntities();


        public ReportDetailByIdResponse GetReportDetailById(ReportDetailByIdRequestModel objReportDetailByIdRequestModel)
        {
            try
            {
                ReportDetailByIdResponse objReportDetailByIdResponse = _objHealthCareEntities.Database.SqlQuery<ReportDetailByIdResponse>("GetReportDetailsById @UserId=@UserId",
                                                   new SqlParameter("UserId", objReportDetailByIdRequestModel.UserId)).FirstOrDefault();

                return objReportDetailByIdResponse;
            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public List<ReportListPatient> GetReportList(ReportListRequestModel objReportListRequestModel)
        {
            try
            {
                
               List<ReportListPatient> objReportListPatient = _objHealthCareEntities.Database.SqlQuery<ReportListPatient>("GetReportList @IsOrderByDate=@IsOrderByDate,@OrderByName=@OrderByName,@DoctorName=@DoctorName,@ClinicName=@ClinicName,@Limit=@Limit,@OffSet=@OffSet",
               new SqlParameter("IsOrderByDate", objReportListRequestModel.IsOrderByDate),
               new SqlParameter("OrderByName", objReportListRequestModel.OrderByName),
               new SqlParameter("DoctorName", objReportListRequestModel.DoctorName),
               new SqlParameter("ClinicName", objReportListRequestModel.ClinicName),
               new SqlParameter("Limit", objReportListRequestModel.Limit),
               new SqlParameter("OffSet", objReportListRequestModel.OffSet)).ToList();

                    return objReportListPatient;
               // }
               
            }
            catch(Exception ex)
            {
                return null;
            }
        }


        public PrescriptionListModel GetPrescriptionList(PrescriptionListRequestModel objPrescriptionListRequestModel, long UserId)
        {

            PrescriptionListModel objPrescriptionListModel = new PrescriptionListModel();
            //PrescriptionListRequestModel objPrescriptionListRequestModel = new PrescriptionListRequestModel();
        
            try
            {
                objPrescriptionListModel.Response.PrescriptionList = _objHealthCareEntities.Database.SqlQuery<PrescriptionList>("GetPrescriptionList @UserId=@UserId,@key=@key",
                             new SqlParameter("UserId", UserId),
                             new SqlParameter("key", objPrescriptionListRequestModel.key)).ToList();


                foreach (var item in objPrescriptionListModel.Response.PrescriptionList)
                {
                   
                    List<TabletList> objTabletList = _objHealthCareEntities.Database.SqlQuery<TabletList>("GetTabletList @PrescriptionId=@PrescriptionId,@key=@key",
                                                 new SqlParameter("PrescriptionId", item.PrescriptionId),
                                                 new SqlParameter("key", objPrescriptionListRequestModel.key)).ToList();
                    item.TabletList = objTabletList;
                }
                return objPrescriptionListModel;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<DepartmentList> GetDepartment()
        {
            try
            {
                List<DepartmentList> objDepartmentList = _objHealthCareEntities.Database.SqlQuery<DepartmentList>("GetDeparment").ToList();

                return objDepartmentList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public List<DoctorByDepartmentList> GetDoctorByDept(Int64 DepartmentId)
        {
            DoctorByDepartmentRequestModel objDoctorByDepartmentRequestModel = new DoctorByDepartmentRequestModel();
            try
            {
                List<DoctorByDepartmentList> objDoctorByDepartmentList = _objHealthCareEntities.Database.SqlQuery<DoctorByDepartmentList>("GetDoctorsByDepartment @DepartmentId=@DepartmentId",
                                                                        new SqlParameter("DepartmentId", DepartmentId)).ToList();
                return objDoctorByDepartmentList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<ReasonforvisitList> GetReasonForVisitList()
        {
            try
            {
                List<ReasonforvisitList> objReasonforvisitList = _objHealthCareEntities.Database.SqlQuery<ReasonforvisitList>("GetReasonForVisit").ToList();

                return objReasonforvisitList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<ReportResponse> GetReportById(ReportListRequestModel objReportListRequestModel)
        {
            try
            {

                List<ReportResponse> model = _objHealthCareEntities.Database.SqlQuery<ReportResponse>("GetReportByUserId @UserId=@UserId",
                    new SqlParameter("UserId", objReportListRequestModel.UserId)).ToList();

                return model;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
